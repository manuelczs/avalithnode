const express = require('express');
const app = express();
const path = require('path');
const fs = require("fs").promises;

app.use(express.json());

app.get('/date', (req, res) => {
  const date = new Date().toDateString();
  res.status(200).send(`Current date: ${date}`);
});

app.get('/data', async (req, res) => {
  try {
    const data = await fs.readFile(path.join(__dirname, 'data.in'), 'utf8');
    res.status(200).send(data);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.post('/greet', (req, res) => {
  const { name } = req.body;
  if(name) {
    res.status(200).send(`Hello, ${name}!`);
  }
  res.status(500).send('undefined');
});

module.exports = app;