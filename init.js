const app = require('./server');
const port = 3000;

const init = () => {
  app.listen(port, () => {
    console.log(`Server running on port: ${port}`);
  });
}

init();