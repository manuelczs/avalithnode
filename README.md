## Instalar dependencias

    npm install

## Iniciar servidor

    npm run start

</br>

# Endpoints

Obtener un archivo: 

    https://localhost:3000/data
</br>

Saludar usando el metodo POST:
    
    https://localhost:3000/tu-nombre-aqui

</br>

Obtener fecha actual: 

    https://localhost:3000/date